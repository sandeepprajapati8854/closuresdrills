function cacheFunction(callbackFunc) {
  let obj = {};
  if (typeof callbackFunc != "function") {
    throw new Error("Partial parameters passed ");
  }

  function invoking(...multipleArgument) {
    if (obj[multipleArgument]) {
      return obj[multipleArgument];
    } else {
      obj[multipleArgument] = callbackFunc(...multipleArgument);
      return obj[multipleArgument];
    }
  }
  return invoking;
}

module.exports = cacheFunction;
