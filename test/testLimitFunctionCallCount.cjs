let limitFunctionCallCount = require("../limitFunctionCallCount.cjs");
let n = 2;
function callbackFunc(num) {
  return num;
}
try {
  let returnFun = limitFunctionCallCount(callbackFunc, n);
  // console.log(typeof nTimes);
  let result = returnFun(4, 4, "Sandeep"); // taking a multiple argument using spread opreator
  console.log(result);
} catch (error) {
  console.log(error);
}
