let counterFactory = require("../counterFactory.cjs");

let returnCounterFunc = counterFactory();
console.log(returnCounterFunc.increment());
console.log(returnCounterFunc.decrement());
