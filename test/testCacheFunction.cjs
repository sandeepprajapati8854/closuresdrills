let cacheFunction = require("../cacheFunction.cjs");

function callbackFunc(name) {
  return name;
}

try {
  const callingCallback = cacheFunction(callbackFunc);
  console.log(callingCallback("Sandeep", "Prajapti"));
  console.log(callingCallback(1, 2, 5, 6));
  console.log(callingCallback("mountblue", "Geekskool"));
} catch (error) {
  console.error(error);
}
