function limitFunctionCallCount(callbackFunc, n) {
  if (typeof n != "number" || n === undefined) {
    throw new Error("Parameter is not a number!");
  }
  if (typeof callbackFunc != "function") {
    throw new Error("Callback is not a function");
  }
  let numberOfLimitedOperatios = 0;
  function invoke(...multipleArguments) {
    if (numberOfLimitedOperatios < n) {
      numberOfLimitedOperatios++;
      return callbackFunc(...multipleArguments);
    } else {
      return null;
    }
  }
  return invoke;
}

module.exports = limitFunctionCallCount;
